# 可复用的Fortran代码准则

> **左志华 zuo.zhihua@qq.com**
>
> **2021-1-22 深圳罗湖**
>
> **哈尔滨工程大学 船舶工程学院**



[toc]

## 1. 介绍

本文档的目的是提供使用Fortran语言的程序框架，从而促进组织间的代码交换。

为了达到这一目的，我们为这些代码文档建立标准，包括软件的内部、外部文档，以及软件本身，同时为代码书写建立标准。

后者的标准被设计用来提高代码的可读性和可维护性，同时尽可能地确保它的可移植性和计算机资源的高效使用。

### 要点

**源代码是给人阅读的，二进制编码是给计算机阅读的！**

**源码可运行，不必要时可不更改。**

## 2. 文档

文档可以尝试分为2类：外部文档（代码以外）；内部文档（代码内部）。

这些将分别在2.1和2.2节中被详细讲述。实用的文档需要在任意研究中心既是最新的又是可读的，而不仅仅在代码被最初生成的环境。因为这些其他研究中心可能希望修改这些被导入的“新”代码文档，包括内部的、外部的，我们希望所有的文档是中文[[1\]](##_ftn1)的。



### 2.1 外部文档

在大多数情况下，这个会被提供在包的级别，而不是每个单独的例程。它必须包括以下：

1. 高级别科学文档：这建立起通过被包和适用性解决方案的科学原理解决的问题。
2. 实施文档：这个文档记录了在科学文档中被描述的解决方法的特定实施。所有包里的例程（子例程、函数、模块等等）应该通过名字和一个描述它们是做什么的简单描述被列出来。在包里，需要有包含所有调用例程的树状列表。
3. 用户指南：详细介绍包里的所有输入。这既包括了被引入该包的子例程参数，又包括在包里的任何开关变量或者可调变量。应给出适当的默认值，合理的值范围等。读取的任何文件或名称列表均应进行详细说明。



### 2.2 内部文档

这将在独立例程级别上应用。有4种类型的内部文档，所有这些都必须存在。

1. 过程标头：每个子例程，函数，模块等都必须具有标头。标头的目的是为了描述例程的功能，可能是通过引用外部文档，并记录例程中使用的变量。所有例程中使用的变量必须在标头中声明，并注释为他们的目的。本标准的要求是附录A中列出的标题被充分使用和完成。如果他们愿意的话，允许中心向这些标题添加额外的部分。

2. 小节注释：这些将代码划分为编号的逻辑部分，可以参考到外部文档。这些评论必须放在自己的被定义的部分的开始位置。部分的注释被推荐的格式是：

```fortran
!--------------------------------------------------------------------------
! <部位编号> <部位标题>
!--------------------------------------------------------------------------
```

<>中的文本可以被适当地替换。

3. 其他注释：这些注释旨在供程序员阅读代码，并旨在简化了解正在发生的事情的任务。这些注释必须放在紧接在其注释的代码之前或与之注释相同的行。这些注释的推荐格式为：

```fortran
! <注释>
```

<>中的文本可以被适当地替换。

4. 有意义的名称：如果使用有意义的词来表示代码构造变量和子程序的名称，则代码可读性更高；不吝啬采取较长的名称，但意义清晰。

   建议所有内部文档均以中文撰写。但是，意识到这并非总是可能的，因此母语注释的替代规则提供带有重复中文注释。

   1. 有意义的名字可以用母语写成。

   2. 注释（见上文）必须有中文。

   3. 英文注释是可选的。

   5. 描述标题部分中声明的变量的注释可以写成母语。带有中文注释的重复声明应为放在一个单独的文件中（包含该程序包的所有此类重复声明）。由于给定的包里重复使用相同的变量名以用于不同的目的是禁止的，可以提供一个简单的工具来替换本地语言移植代码的源文件中带有中文声明的声明。



## 3. 包的编码规则

这些规则大致基于Kalnay等人的“插头兼容性”规则（1989）。本文档中其他地方出现的规则在本节中没有重复。

包应仅引用包含在Fortran 90标准中其自身的模块和子程序以及那些固有例程。
 很有可能需要扩展它以包括（在本文档中）命名的库，例如NAG库。我们甚至可以举行联合会议。可能包含内容的实用程序库例如多重网格p.d.e.求解器或用于计算饱和蒸气压的例程……

软件包应提供单独的设置和运行过程，每个过程都有一个入口点。静态数据的所有初始化都必须在设置过程中完成，并且该数据不得由运行过程更改。

与包的外部通讯必须通过以下方式进行： 

1. 软件包入口和设置例程的参数列表。
2. 读取一个`NAMELIST`文件。
3. 由一组标准例程读取的环境变量。环境变量必须记录在文档中并由适合所使用操作系统的脚本设置（即Unix操作系统的posix脚本）。
          我们还可以允许使用本文档中定义的标准模块，包含有关网格类型和分辨率等的信息；错误处理...
4. 软件包内部可以使用模块--例如设置和运行程序可以通过这种方式进行通信。
5. 必须提供用于设置和运行过程的接口块（可能通过模块）。这允许使用假定的形状数组，可选参数等以及允许编译器检查实际和伪参数类型匹配。如果这些外部参数列表中的变量属于派生类型，则模块必须被提供，其中包含进行这些操作所需的类型定义语句让派生类型可用于调用程序包的例程。
6. 程序包不得终止程序执行。如果内部发生任何错误打包它应该正常退出，并通过一个整数变量在外部报告其错误参数列表（+ve=致命错误）。软件包还应该写出诊断消息使用Fortran I/O将问题描述为可通过以下方式选择的“错误流”单元号程序包的设置例程。
          请注意，如果程序包是从unix脚本而不是Fortran开始的，则级别为正常退出包括通过使用在包的Fortran部分中`STOP`语句将控制返回到程序包的脚本级别。
7. 软件包的书写应尽可能独立于分辨率。分辨率必须通过包的设置例程进行调整。
8. 预编译器：例如，这些用于提供选择（或取消选择）部分代码进行编译。显然，为了简化代码的可移植性，我们
          都需要使用相同的预编译器，并且每个中心都可以使用。C预编译器可能是最好的选择，因为它将在使用UNIX操作系统。
          将其作为标准的预编译器会因为不同的中心而存在一些问题，致力于不同的预编译器。但是，这可能不是一个太大的任务让每个中心从当前的预编译器转换为C的预编译器。
9. 所有unix脚本必须使用posix shell编写。这是一个标准化的外壳，在所有POSIX兼容的UNIX系统上可用，具有许多有用的功能（**Windows系统也支持Linux系统shell命令行了**）。
10. 每个程序单元应存储在单独的文件中。 



## 4. 动态内存使用指南

强烈希望使用动态内存，因为从原则上讲，它允许一组编译代码以任何指定的分辨率工作（或至少达到硬件内存限制）；并允许高效重用工作空间内存。但是，必须小心，因为可能会导致内存效率低下用法，尤其是在并行化代码中。例如，如果分配了空间，则可能发生堆碎片由较低级别的例程执行，然后在将控制权传递回调用树之前未释放。在Fortran 90中有获得动态内存的三种方法：

1. 自动数组：这些是最初在子程序中声明的数组，其范围取决于运行时已知的变量，例如：通过其参数列表传递给子程序的变量。

2. 指针数组：使用`POINTER`属性声明的数组变量可以在运行时使用`ALLOCATE`命令分配空间。

3. 可分配的数组：使用`ALLOCATABLE`属性声明的数组变量可以在运行时通过使用`ALLOCATE`命令分配空间。但是，与指针不同的是，派生数据类型内部不允许分配。

   + 与其他形式的动态内存分配相比，优先使用自动数组。

   -    必须使用`DEALLOCATE`语句显式释放使用上述b）和c）分配的空间。

   -   在给定的程序单元中，请勿重复分配空间，请先取消分配空间，然后再分配较大的空间块。这几乎肯定会生成大量不可用的内存。

   -   始终测试动态内存分配和释放的成功。 `ALLOCATE`和`DEALLOCATE`语句具有一个可选参数，可让您执行此操作。 



## 5. 例程的编码规则

例程是指任何Fortran程序单元，例如子例程，函数，模块或程序。这些规则旨在通过建立一些基本的通用样式规则来鼓励良好的结构化编程实践，简化维护任务并简化交换代码的可读性。



### 5.1 禁止的Fortran功能

以下某些部分详细介绍了Fortran 90不推荐使用的功能或冗余功能。其他一些功能禁止使用被认为是不好的编程习惯的功能，因为它们会降低代码的可维护性。

1. `COMMON`块-改用`Module`。

2. 等效-改为使用`POINTERS`或派生数据类型。

3. 分配和计算的`GO TO`-使用`CASE`构造。

4. 算术`IF`语句-改为使用块`IF`构造。

5. 标签（仅允许使用一种）。

   -   带标签的`DO`构造-改为使用`END DO`。

   -   I/O例程的End=和`ERR=`使用`IOSTAT`代替。

   -   `FORMAT`语句：在`Read`或`Write`语句内使用`Character`参数或显式格式说明符。

   -   `GO TO`

      `GO TO`的唯一公认用途（实际上是标签）是在检测到错误时跳到例程末尾的错误处理部分。跳转必须是一个`CONTINUE`语句，并且所使用的标签必须是`9999`。即使这样，建议避免这种做法。

      可以通过使用`IF`，`CASE`，`DO WHILE`，`EXIT`或`CYCLE`语句来避免对`GO TO`进行任何其他使用。如果确实需要使用`GO TO`，则请用清楚的注释将其解释为正在发生的情况，并在类似注释的`CONTINUE`语句上终止跳转。

6. `PAUSE`

7. `ENTRY`语句：子程序只能有一个入口点。

8. 具有副作用的函数，即更改其参数列表或该函数使用的模块中的变量的函数；或执行I / O操作的一个。这在C编程中很常见，但可能会造成混淆。同样，如果编译器知道函数没有副作用，则可以提高效率。高性能Fortran是为大型并行计算机设计的Fortran 90的变体，将允许使用此类指令。

9. 在将数组传递到子例程时隐式更改数组的形状。尽管在标准中实际上是被禁止的，但是在FORTRAN 77中非常普遍的做法是将“n”维数组传递到子例程中，在该子例程中将其视为一维数组。尽管在Fortran 90中已禁止这种做法，但对于没有提供接口块的外部例程仍然可以实现。这样做仅是基于对数据存储方式的假设：因此，它不太可能在大型并行计算机上工作。因此，这种做法被禁止。



### 5.2 样式规则

一般的思想是编写易于阅读和维护的可移植代码。代码应以尽可能通用的方式编写，以允许进行不可预见的修改。实际上，这意味着编码将花费更长的时间。这笔额外的工作花了很多钱，但是，由于在整个软件生命周期内都将减少维护成本。

1. 使用自由格式语法。

2. 允许的最大行长为**80**个字符。 Fortran 90允许的行长最多为132个字符，但是，如果在较旧的终端上查看，或者必须在`A4`纸上打印输出，则可能会导致问题。

3. 在所有程序单元中均不得使用隐式任何内容。这样可以确保必须明确声明所有变量，并进行记录。它还允许编译器检测变量名称中的印刷错误。

4. 使用有意义的变量名，最好使用英文。公认的缩写可以防止变量名过长，建议合理组合使用**驼峰式命名和匈牙利命名**。

5. Fortran语句只能用大写字母或首字母大写，其余用小写字母写。变量，参数，子例程等的名称可以用混合的（大多数情况是小写的）形式编写（**遗弃**）。

6. 提高`DO`内部的缩进代码的可读性；当做; `IF`块; `case`; `interface`; etc由2个字符构成。

7. 缩进连续线以确保例如多线方程的各个部分以可读的方式排列。

8.  如果它们出现在单独的行上，则缩进类型c）内部注释，以反映代码的结构。如果用少于一个字符的方式完成此操作，则缩进注释与代码明显分开，但不要破坏其结构。

9.  在水平和垂直方向使用空格，以提高可读性。尤其要在变量和运算符之间留出空白，并尝试将相关代码排列到各列中。例如，

   代替：

   ```fortran
! Initialize Variables
x=1
MEANINGFUL_NAME=3.0
SILLY_NAME=2.0
write:
! Initialize variables
x = 1
MeaningfulName = 3.0
SillyName = 2.0
   ```

   同样，尝试使方程式可识别并可读为方程式。通过在放置在适当列中的操作符处开始续行而不是在操作符处结束续行可以大大提高可读性。

10. 请勿在代码中使用制表符（**使用4格空格代替**）：这将确保代码在移植时看起来符合预期。
11. 将有关如何在I/O语句上输出的信息与格式信息分开。就是不要在I/O语句的括号内放置文本。
12. 删除未使用的标题组件。
13. 本文档的许多作者强烈希望包含建议的变量命名约定。但是，已决定不适合使用Fortran 77约定，并且在指定适当的约定之前需要更多的Fortran 90经验。旨在将这种约定包含在本文档的修订版中。



### 5.3 使用新的Fortran功能

不可避免地会有“愚蠢”的方式来使用Fortran 90中引入的新功能。显然，我们可能希望禁止此类使用，并建议某些实践优先于其他实践。但是，在我们获得足够的经验以完整列出此类建议之前，必须对新语言进行一定程度的试验。根据这些经验，必须修改和扩展以下规则。

1. 我们建议仅使用`Use`来指定模块中定义的哪些变量，类型定义等可用于Using例程。

2. 讨论接口块的使用：

**介绍**

如果要使用可选参数或关键字参数，则在f90例程之间需要显式接口块。它们还允许编译器检查CALL中指定的参数的类型，形状和数量是否与子程序本身中指定的参数相同。另外，某些编译器（例如Cray f90编译器）使用接口块的存在来确定被调用的子程序是否用f90编写（这改变了将数组信息传递给子例程的方式）。因此，通常希望在f90例程之间提供显式接口块。执行此操作的方法有多种，每种方法都对程序设计有影响。代码管理；甚至配置控制。以下各节讨论了三个主要选项：

**选项I：明确编码的接口块**

接口块可以显式地写入调用例程中，实质上是通过从被调用例程中复制参数列表声明部分来实现的。但是，这种直接方法有一些缺点。首先，它增加了维护调用例程所需的工作量，这似乎使被调用例程的参数列表发生了变化，因此必须更新接口块以及`CALL`。此外，不能保证调用例程中的`Interface`块实际上是最新的，并且与被调用例程的实际接口相同。

**选项II：模块中的显式编码接口块**

包中所有例程的接口块都可以显式地写入模块中，并且包中所有例程都使用此模块。这样做的好处是，可以检查一个例程以查找所有例程的接口规范-这可能比单独检查所有被调用例程的源代码容易。但是，除了例程本身和对其的`CALL`调用之外，还必须通过程序或其他方法来维护`Interface`块。可以编写unix脚本来自动生成包含接口块的模块。

**选项III：自动接口块**

Fortran 90编译器可以在`Contains`语句之后自动在例程之间提供显式接口块。接口模块还提供给任何常规的使用模块的模块。因此，可以设计一个系统，在该系统中，实际上没有对接口块进行编码，而编译器在所有例程之间都提供了显式接口块。一种实现方法是在f90模块级别“模块化”代码，即在`Contains`语句之后将相关代码放置在一个模块中。然后，模块A中的例程a只需在模块B中调用例程b，即可使用模块B自动向例程b提供显式接口。显然，如果例程b在模块a中，则不需要使用。这种方法的一个后果是模块和模块中包含的所有例程组成一个编译单元。如果模块很大，或者包中的每个模块都包含在包中使用许多其他模块的例程，那么这可能是不利的（在这种情况下，更改一个模块中的一个例程将需要重新编译整个包）。另一方面，大大减少了编译单元的数量，简化了编译和配置控制系统。

**选项IV：BLOCK**

Fortran 08编译器可以为本地局部添加`BLOCK`，这一项语法的新增，可以一定程度解决“变量灾难”的问题，合理地使用它能使程序条理清晰。

**结论**

选项II和III都为显式接口块问题提供了可行的解决方案。选项III可能更可取，因为编译器会完成提供接口块的工作，减少编程开销，并同时确保所使用的接口块正确。选择哪个选项将对代码管理和配置控制以及程序设计产生重大影响。

3. 尽可能使用数组符号。这将有助于优化，并减少所需的代码行数。为了提高可读性，请在方括号中显示数组的形状，例如：

```fortran
1dArrayA(:) = 1dArrayB(:) + 1dArrayC(:)
2dArray(:, :) = scalar * Another2dArray(:, :)
```



4. 当访问数组的子部分时，例如在有限差分方程中，请在整个数组上使用三元组表示法，例如：

```fortran
2dArray(:, 2:len2) = scalar & ! 合理使用'&'，避免造成阅读困难和单句过长

           \* ( Another2dArray(:, 1:len2 -1) &

           \- Another2dArray(:, 2:len2) &

            )
```



5. 始终命名“程序单位”并始终使用“结束”程序；结束子程序；终端接口；终端模块等构造，再次指定“程序单元”的名称。

6. 使用`>`，`>=`，`==`，`<`，`<=`，`/=`代替`.gt.`，`.ge.`，`.eq.`，`.lt.`，`.le.`，`.ne.`在逻辑上比较。与标准数学符号更接近的新语法应该更清晰。

7. 不要在一行上放置多个语句：这会降低代码的可读性。

8. 变量声明：如果我们都采用相同的约定来声明变量，则将提高可理解性，因为Fortran 90提供了许多不同的语法来实现相同的结果。

   -   不要使用`DIMENSION`语句或属性：在声明语句中的变量名称后，声明括号内数组的形状和大小。

   -   始终使用::表示法，即使它们不是属性。

   -   使用`(len =)`语法声明字符变量的长度。

9. 我们建议不要出于效率的考虑而使用递归例程（它们在使用cpu和内存时往往效率低下）。

10. 建议定义新的运算符，而不要重载现有的运算符。这将更清楚地记录正在发生的事情，并应避免降低代码的可读性或可维护性。

11. 为了提高32位和64位平台之间的可移植性（**可选**。在2021年，64位机器和64位软件已经占据主流），利用各种类型以获得所需的数值精度和范围非常有用。应该编写一个模块来定义与每种所需种类相对应的参数，例如：

```fortran
integer, parameter :: single & ! single precision kind.

            = selected_real_kind(6,50)

integer, parameter :: double & ! double precision kind.

            = selected_real_kind(12,150)
```

然后可以在每个例程中使用该模块，从而允许使用适当的类型（例如）声明所有变量。

```fortran
real(single), pointer :: geopotential(:,:,:) ! Geopotential height fields
```



### 5.4 执行这些标准

确保遵守这些标准显然很重要-特别是使文档与软件保持最新；并且该软件以尽可能可移植的方式编写。如果不遵守这些标准，则代码的可交换性将受到损害。可能可以对软件工具（例如QA fortran）进行量身定制，以测试是否符合这些标准。这需要调查。

一种选择是建立一个可交换代码及其外部文档的中央数据库。接受标准是满足本文档中列出的标准。当然，这需要资金来提供硬件，软件和人员来维护数据库。

在另一个极端，每个中心都可以采用自己的执行策略，并将其自己的可交换软件列表分发给其他中心。

最好的解决方案很可能是设计一个利用Mosaic和Internet的分布式系统。



## 6. 参考文献

Kalnay et al. (1989) "Rules for Interchange of Physical Parametrizations" Bull. A.M.S., 70 No. 6, p620.



## 附录A：修改历史记录：

23/3/94: Draft Version 0.1 Phillip Andrews (UKMO)

22/4/94: Draft Version 0.2 Phillip Andrews (UKMO)

7/6/94: Draft Version 0.3 Phillip Andrews (UKMO)

31/7/94: Draft Version 0.4 Phillip Andrews (UKMO)

29/9/94: Draft Version 0.5 Phillip Andrews (UKMO)

14/10/94: Draft Version 0.6 Phillip Andrews (UKMO)

Renumbered Version 1.0

20/6/95: Version 1.1 Phillip Andrews (UKMO)

16/12/20: Version 1.2 Zuo Zhihua (HEU) .Chinese Version



## 附录B：标准标头

标准标题显示在此附录中。它们被编写为模板。用户必须将<>括号内的文本替换为适当的文本。



### 程序标头

```fortran

!+ <A one line description of this program>

!

PROGRAM <NameOfProgram>

 ! 描述[Description]:

 ! <说这个程序做什么的>

 ! <Say what this program does>

 !

 ! 方法[method]:

 ! <说明其操作方式：包括对外部文档的引用>

 ! <如果将此例程划分为多个部分，请在此处进行简要说明，

 ! 并将“方法注释”放在每个部分的开头>

 ! <Say how it does it: include references to external documentation>

 ! <If this routine is divided into sections, be brief here,

 ! and put Method comments at the start of each section>

 !

 ! 输入文件[Input files]:

 ! <描述这些文件，并说明在哪个例程中读取它们>

 ! <Describe these, and say in which routine they are read>

 ! 输出文件[Output files]:

 ! <描述这些，并说它们写在哪个例程中>

 ! <Describe these, and say in which routine they are written>

 ! 当前代码所有者[Current Code Owner]: 

 ! <负责此代码的人员名称> <Name of person responsible for this code>

 ! 历史[History]:

 ! Version  Date  CommentCN[CommentEN]

 ! ---------  ----   ---------------- 

 ! <version> <date> 原始代码[Original code]。 <你的名字[Your name]>

 ! 代码说明[Code Description]: 语言[Language] => Fortran 90.

 ! 软件标准[Software Standards]: “书写和存储可复用的Fortran90代码准则”.

 !                https://www.jianshu.com/p/3c7f45cde3ea

 ! <声明[Declarations]>

 ! 使用的模块[Modules used]:

 USE, ONLY : &

 ! <导入的类型定义[Imported Type Definitions]>

 ! 导入的参数[Imported Parameters]:

 ! <导入的标量/数组变量（输入/输出）[Imported Scalar Variables/Arrays, intent (in/out)]>

 ! 导入的例程[Imported Routines]:

 ! <重复每个模块的使用...>

 ! <Repeat from Use for each module...>

 IMPLICIT NONE

 ! <包含陈述[Include statements]>

 ! <声明必须采用以下形式[Declarations must be of the form]>

 ! <type> <VariableName>! 变量说明/用途

 !            ! Description/ purpose of variable

 ! <本地常数/标量/数组[Local parameters/scalars/arrays]>

END PROGRAM <NameOfProgram>

!- 程序标头末尾[End of program header] -----------------------------------------
```



### 模块标头

```fortran

!+ <A one line description of this module>

!

MODULE <ModuleName>

 !

 ! 描述[Description]:

 ! <说这个模块做什么的>

 ! <Say what this module is for>

 !

 ! 当前代码所有者[Current Code Owner]: 

 ! <负责此代码的人员名称> <Name of person responsible for this code>

 !

 ! 历史[History]:

 ! Version  Date  CommentCN[CommentEN]

 ! ---------  ----   ---------------- 

 ! <version> <date> 原始代码[Original code] <你的名字[Your name]>

 ! 代码说明[Code Description]:

 ! 语言[Language]: Fortran 90.

 ! 软件标准[Software Standards]: “可复用的Fortran代码准则”.

 !                https://www.jianshu.com/p/3c7f45cde3ea

 ! 声明[Declarations]:

 ! 使用的模块[Modules used]:

 USE, ONLY : &

 ! 导入的类型定义[Imported Type Definitions]：

 ! 导入的参数[Imported Parameters]：

 ! 导入的标量/数组变量（输入/输出）[Imported Scalar/arrays Variables, intent (in/out)]：

 ! 导入的例程[Imported Routines]：

 ! <重复每个模块的使用...>

 ! <Repeat from Use for each module...>

 IMPLICIT NONE

 ! 包含陈述[Include statements]:

 ! 声明必须采用以下形式[Declarations must be of the form]：

 ! <type> <VariableName> ! 变量说明/用途

 !            ! Description/ purpose of variable

 ! 全局声明[Global (i.e. public) Declarations]:

 ! 全局类型定义[Global Type Definitions]:

 ! 全局常量/标量/数组[Global Parameters/Scalars/Arrays]:

 ! 本地声明[Local (i.e. private) Declarations]:

 ! 本地类型定义[Local Type Definitions]:

 ! 本地常量/标量/数组[Local Parameters/Scalars/Arrays]:

 ! 运算符定义[Operator definitions]:

 ! <定义新的运算符或重载现有的运算符。 >

 ! <Define new operators or overload existing ones.>

CONTAINS

 ! <定义此模块中包含的过程。>

 ! <Define procedures contained in this module.>

END MODULE <ModuleName>

!- 模块标头末尾[End of module header] ------------------------------------------
```



### 子例程标头

```fortran

!+ <A one line description of this subroutine>

!

SUBROUTINE <SubroutineName> &

!

(<InputArguments, inoutArguments, OutputArguments>)

 !

 ! 描述[Description]:

 ! <说这个子例程做什么的>

 ! <Say what this routine does>

 !

 ! 方法[Method]:

 ! <说明其操作方式：包括对外部文档的引用>

 ! <如果将此例程划分为多个部分，请在此处进行简要说明，

 ! 并将“方法注释”放在每个部分的开头>

 ! <Say how it does it: include references to external documentation>

 ! <If this routine is divided into sections, be brief here,

 ! and put Method comments at the start of each section>

 !

 ! 当前代码所有者[Current Code Owner]: 

 ! <负责此代码的人员名称> <Name of person responsible for this code>

 !

 ! 历史[History]:

 ! Version  Date  CommentCN[CommentEN]

 ! ---------  ----   ---------------- 

 ! <version> <date> 原始代码[Original code] <你的名字[Your name]>

 ! 代码说明[Code Description]: Fortran.

 ! 软件标准[Software Standards]: “可复用的Fortran代码准则”.

 !                https://www.jianshu.com/p/3c7f45cde3ea

 ! 声明[Declarations]:

 ! 使用的模块[Modules used]:

 USE, ONLY : &

 ! 导入的类型定义[Imported Type Definitions]：

 ! 导入的参数[Imported Parameters]：

 ! <导入的标量/数组变量（输入/输出）>

 ! <[Imported Scalar/Array Variables, intent (in/out)]>

 ! 导入的例程[Imported Routines]：

 ! <重复每个模块的使用...>

 ! <Repeat from Use for each module...>

 IMPLICIT NONE

 ! 包含陈述[Include statements]:

 ! 声明必须采用以下形式[Declarations must be of the form]：

 ! <type> <VariableName>! 变量说明/用途

 !            ! Description/ purpose of variable

 ! 子例程参数[Subroutine arguments]：

 ! <标量/数组参数（输入/输入输出/输出）>

 ! [Scalar/Array arguments, intent(in/inout/out)]>

 ! 本地常量/标量/数组[Local parameters/scalars/arrays]:

END SUBROUTINE <SubroutineName>

!- 子例程末尾标头[End of subroutine header] -----------------------------------
```


### 函数标头

```fortran

!+ <A one line description of this function>

!

FUNCTION <FunctionName> &

(<InputArguments>) &

RESULT (<ResultName>) ! The use of result is recommended

! but is not compulsory.

 ! 描述[Description]:

 ! <说这个函数做什么的>

 ! <Say what this function does>

 !

 ! 方法[Method]:

 ! <说明其操作方式：包括对外部文档的引用>

 ! <如果将此例程划分为多个部分，请在此处进行简要说明，

 ! 并将“方法注释”放在每个部分的开头>

 ! <Say how it does it: include references to external documentation>

 ! <If this routine is divided into sections, be brief here,

 ! and put Method comments at the start of each section>

 !

 ! 当前代码所有者[Current Code Owner]: 

 ! <负责此代码的人员名称> <Name of person responsible for this code>

 !

 ! 历史[History]:

 ! Version  Date  CommentCN[CommentEN]

 ! ---------  ----   ---------------- 

 ! <version> <date> 原始代码[Original code]  <你的名字[Your name]>

 ! 代码说明[Code Description]:

 ! 语言[Language]: Fortran 90.

 ! 软件标准[Software Standards]: “书写和存储可复用的Fortran90代码准则”.

 !                https://www.jianshu.com/p/3c7f45cde3ea

 ! 声明[Declarations]:

 ! 使用的模块[Modules used]:

 USE, ONLY : &

 ! 导入的类型定义[Imported Type Definitions]：

 ! 导入的参数[Imported Parameters]：

 ! <导入的标量/数组变量（输入/输出）[Imported Scalar/Arrays Variables with intent (in/out)]> 

 ! 导入的例程[Imported Routines]：

 ! <重复每个模块的使用...>

 ! <Repeat from Use for each module...>

 IMPLICIT NONE

 ! 包含陈述[Include statements]:

 ! 声明必须采用以下形式[Declarations must be of the form]：

 ! <type> <VariableName>! 变量说明/用途

 !            ! Description/ purpose of variable

 ! 子例程参数[Subroutine arguments]：

 ! <标量/数组参数（输入）[Scalar/Array arguments with intent(in)]>

 ! <本地常量/标量/数组[Local parameters/scalars/arrays]>

END FUNCTION <FunctionName>


```
```markdown
------

[[1\]](##_ftnref1) 原文此处为English，翻译为中文。

```

## 附录C: Fortran设计模式

### 1. [设计模式介绍](https://refactoringguru.cn/design-patterns)

#### 关于模式的争议

设计模式自其诞生之初似乎就饱受争议， 所以让我们来看看针对模式的最常见批评吧。

##### 一种针对不完善编程语言的蹩脚解决方案 

通常当所选编程语言或技术缺少必要的抽象功能时， 人们才需要设计模式。 在这种情况下， 模式是一种可为语言提供更优功能的蹩脚解决方案。

例如， [策略](https://refactoringguru.cn/design-patterns/strategy)模式在绝大部分现代编程语言中可以简单地使用匿名 （lambda） 函数来实现。

##### 低效的解决方案

模式试图将已经广泛使用的方式系统化。 许多人会将这样的统一化认为是某种教条， 他们会 “全心全意” 地实施这样的模式， 而不会根据项目的实际情况对其进行调整。

##### 不当使用

>   **如果你只有一把铁锤， 那么任何东西看上去都像是钉子。**

这个问题常常会给初学模式的人们带来困扰： 在学习了某个模式后， 他们会在所有地方使用该模式， 即便是在较为简单的代码也能胜任的地方也是如此。

### 2. [Fortran设计模式开源学习代码](https://github.com/farhanjk/FortranPatterns)

Implementation of popular design patterns in Fortran. This is intended to facilitate the community using Fortran for computationally expensive tasks. The main philosophy here is to make these patterns available for improving the reusability and efficiency of the code.

>   **译文：在Fortran中实现流行的设计模式。这旨在促进社区使用Fortran完成计算量大的任务。这里的主要原理是使这些模式可用于提高代码的可重用性和效率。**

Tested on: GNU Fortran (GCC) 4.8.